# Created by newuser for 5.8
#
export EDITOR="nvim"
export PATH=$PATH:$HOME/.local/bin

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/z-a-rust \
    zdharma-continuum/z-a-as-monitor \
    zdharma-continuum/z-a-patch-dl \
    zdharma-continuum/z-a-bin-gem-node

### End of Zinit's installer chunk


#####################
# PROMPT            #
#####################
zinit ice pick"async.zsh" src"pure.zsh"
zinit light sindresorhus/pure

#####################
# PLUGINS           #
#####################
zinit ice wait"0c" lucid atinit"zpcompinit;zpcdreplay"
zinit light zdharma-continuum/fast-syntax-highlighting

zinit ice wait"0a" lucid atload"_zsh_autosuggest_start"
zinit light zsh-users/zsh-autosuggestions

zinit ice wait"0a" lucid blockf atpull'zinit creinstall -q .'
zinit light zsh-users/zsh-completions

zinit ice lucid wait'0a' multisrc"shell/{completion,key-bindings}.zsh" id-as"junegunn/fzf_completions" pick"/dev/null"
zinit light junegunn/fzf

#####################
# HISTORY           #
#####################
[ -z "$HISTFILE" ] && HISTFILE="$HOME/.zhistory"
HISTSIZE=290000
SAVEHIST=$HISTSIZE

#####################
# SETOPT            #
#####################
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_all_dups   # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt share_history          # share command history data
setopt always_to_end          # cursor moved to the end in full completion
setopt hash_list_all          # hash everything before completion
# setopt completealiases        # complete alisases
setopt always_to_end          # when completing from the middle of a word, move the cursor to the end of the word
setopt complete_in_word       # allow completion from within a word/phrase
setopt nocorrect                # spelling correction for commands
setopt list_ambiguous         # complete as much of a completion until it gets ambiguous.
setopt nolisttypes
setopt listpacked
setopt automenu
unsetopt BEEP
setopt vi

#####################
# BINDKEYS          #
#####################
bindkey '^ ' autosuggest-accept

#####################
# FZF SETTINGS      #
#####################
export FZF_DEFAULT_OPTS="
--ansi
--layout=default
--info=inline
--height=50%
--multi
--preview-window=right:50%
--preview-window=sharp
--preview-window=cycle
--preview '([[ -f {} ]] && (bat --style=numbers --color=always --theme=gruvbox-dark --line-range :500 {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--prompt='λ -> '
--pointer='|>'
--marker='✓'
--bind 'ctrl-e:execute(nvim {} < /dev/tty > /dev/tty 2>&1)' > selected"
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

#####################
# NNN               #
#####################
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    #export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    nnn "$@"
    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

#####################
# ALIASES           #
#####################
alias vim="nvim"
alias v="nvim"
alias n="n -de"
alias lsa='ls -A'
alias yt="youtube-dl -ic"
alias yta="youtube-dl -xic"
#alias youtube-mp3="youtube-dl --extract-audio --audio-format mp3 --audio-quality 320k -o '~/Music/recent/%(title)s.%(ext)s'"
alias youtube-video="youtube-dl -i --no-mtime --prefer-free-formats -c -o '~/Videos/recent/%(title)s.%(ext)s'"
alias fullclean="make clean && rm -f config.h && git reset --hard origin/master"
alias h="cd ~ && ls -a"
alias w="cd ~/wallpapers && ls -a"
alias cfb="nvim ~/.bashrc"
alias cfv="nvim ~/.config/nvim/init.vim"
alias cfT="nvim ~/.config/Scripts/tmux.conf"
alias cfs="nvim ~/.config/sxhkd/sxhkdrc"
alias cfsb="nvim ~/.config/sxhkd/sxhkdrc_bspwm"
alias cfB="nvim ~/.config/bspwm/bspwmrc"
alias cfz="nvim ~/.zshrc"
alias gst="git status"

#alias burn="ssh kdamani@10.0.5.4"
alias b="ssh kdamani@burne.sat"
alias dns="ssh administrator@ctrlf-dns"
alias pyans="source ${HOME}/.venv/ansible/bin/activate"
alias pykep="source ${HOME}/.venv/kepler36/bin/activate"
