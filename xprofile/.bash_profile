#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

#if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
	#startx
#fi

#[ -z "$DISPLAY" -a "$(fgconsole)" -eq 1 ] && exec startx

export PATH=$PATH:$HOME/.config/Scripts
export XDG_CONFIG_HOME="$HOME/.config"
export BROWSER="firefox"
export EDITOR="nvim"
export TERMINAL="st"
export READER="zathura"
export QT_QPA_PLATFORMTHEME="qt5ct"
