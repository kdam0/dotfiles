#!/bin/sh

maim --hidecursor --window $(xdotool getactivewindow) /tmp/wall.png
convert /tmp/wall.png -paint 3 /tmp/wall.png

light-locker-command --lock
