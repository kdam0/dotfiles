#!/bin/bash

#Feed this script either:
#	"l" for laptop screen only,
#	"h" for hdmi screen only,
#	"v" for vga screen only,
#	or "d" for dual hdmi/vga/laptop.

MONS=$(xrandr --query | grep -w 'connected' | cut -d ' ' -f1)
MON_COUNT=$(echo "$MONS" | wc -l)
PRIMARY=$(echo "$MONS" | head -n 1)

d() { if [[ "$MON_COUNT" -gt 1 ]]
	then param "$1"
	else echo "No secondary input detected."
	fi ;}

dual() { if [[ "$MON_COUNT" = 3 ]]
	 then xrandr --output "$PRIMARY" --primary --auto --output DP-1 --auto --left-of "$PRIMARY" --output HDMI-2 --auto --right-of "$PRIMARY" ; # office
	 else xrandr --output "$PRIMARY" --primary --auto --output DP-1 --auto --right-of "$PRIMARY"; # home
	 fi ;}
laptop() { xrandr --output "$PRIMARY" --primary --auto --output HDMI-2 --off --output DP-1 --off --output VGA-1 --off ;}
hdmi() { xrandr --output HDMI-2 --auto --output "$PRIMARY" --off ;}
vga() { xrandr --output VGA-1 --auto --output "$PRIMARY" --off ;}
#mirror() { xrandr --addmode VGA1 $lapres && xrandr --output LVDS1 --mode $lapres --output VGA1 --mode $lapres ;}

param() {
case $1 in
	d) dual ;;
	l) laptop ;;
	h) hdmi ;;
	v) vga ;;
	*) echo -e "Invalid parameter. Add one of the following:\n\"d\" for dualscreen laptop and VGA/HDMI.\n\"l\" for laptop only\n\"v\" for VGA only.\n\"h\" for HDMI only." ;;
esac ;}
d $1
