#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# get the os env.
FEDORA=$(hostnamectl | grep -qw Fedora && echo $?)

# Launch in all monitors
MONS=$(polybar -m | sed -e 's/:.*$//g')
for MON in ${MONS[@]}
do
    if [ "$FEDORA" == 0 ];
    then
        MONITOR="$MON" polybar mybar -q -c ~/.config/polybar/mybar_fedora &
    else
        MONITOR="$MON" polybar mybar -q -c ~/.config/polybar/mybar &
    fi
done
