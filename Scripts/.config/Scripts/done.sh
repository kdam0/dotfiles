#!/bin/sh

if [ -e ~/.config/wal/update_bspwm.sh ]; then
  ~/.config/wal/update_bspwm.sh
fi

if [ -e ~/.config/wal/update_dunst.sh ]; then
  ~/.config/wal/update_dunst.sh
fi
