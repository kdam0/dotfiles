#!/bin/bash

function run {
  SERVICE="$@"
  RUNNING=$(ps aux | grep -v grep | grep "$SERVICE")
  if [ "${RUNNING:-null}" = null ];
  then
    $@ &
  fi
}

# Common programs
#~/.config/Scripts/screen.sh d && sleep 2s
xmodmap -e "keycode 135 = Super_R"
#setxkbmap -option caps:escape # NO LONGER NEEDED AFTER: `localectl set-x11-keymap us pc105 '' caps:escape`
xset s off -dpms
run dunst
run nm-applet
run blueman-applet
run picom --experimental-backends --config ~/.config/compton.conf
run flatpak run com.github.wwmm.easyeffects
run rsblocks
#setbg ~/.config/wall.png
#setbg "$WALLPAPERS_HOME"

FEDORA=$(hostnamectl | grep -qw Fedora && echo $?)
if [ "$FEDORA" == 0 ];
then
    xinput --set-prop "Synaptics TM3276-022" "Device Enabled" 0
    run $BROWSER
    run light-locker
else
    synclient TouchpadOff=1
    run /usr/bin/xflux -l 43.6404 -g -79.3995
    run st -c email -e neomutt
    run $BROWSER
    run mpd
    run light-locker
fi

run keepassxc
amixer -c 0 set Headphone 100% unmute
run seafile-applet

# WM specific
if [ $GDMSESSION == "dwm" ];
then
  pkill sxhkd
  run sxhkd -c $HOME/.config/sxhkd/sxhkdrc
  run stalonetray
else
  run ~/.config/Scripts/launch_polybar.sh
  run st -c dropdown -e tmux -f ~/.config/tmux/tmux_start.conf attach
  run st -c math -e bc
fi

