require('Comment').setup{
    padding = true,
    sticky = true,
    ignore = nil,
    toggler = {
        line = '<Leader>cc',
        block = '<Leader>cb',
    },
    opleader = {
        line = '<Leader>c',
        block = '<Leader>b',
    },
}
