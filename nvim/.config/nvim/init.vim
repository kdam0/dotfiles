" General

let mapleader = ","

set termguicolors
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set number
set numberwidth=1
set relativenumber
set signcolumn=yes
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set nohlsearch
set ignorecase
set smartcase
set nowrap
set splitbelow
set splitright
set hidden
set scrolloff=999
set noshowmode
set updatetime=250
set encoding=UTF-8
set pastetoggle=<Leader>v
set showbreak=↪\
set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨
set background=dark

" Plugins

" Auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall
endif
" Enable plugins
call plug#begin('~/.config/nvim/autoload/plugged')
" General
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
Plug 'mcchrish/nnn.vim'
Plug 'lukas-reineke/indent-blankline.nvim'
" Lsp
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Lsp w/ Autocompletion
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/nvim-cmp'
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
" Per-Project LSP settings.
Plug 'tamago324/nlsp-settings.nvim'
" Lsp w/ Snippets
Plug 'rafamadriz/friendly-snippets'
" Docs
Plug 'danymat/neogen'
Plug 'numToStr/Comment.nvim'
" Lsp w/ auto pairs
Plug 'windwp/nvim-autopairs'
" Telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
call plug#end()
" Source plugin configs.
lua require('kdam0')

" n³
let g:nnn#layout = { 'window': { 'width': 0.9, 'height': 0.6, 'highlight': 'Debug' } }
let g:nnn#action = {
  \ '<c-t>': 'tab split',
  \ '<c-x>': 'split',
  \ '<c-v>': 'vsplit' }
let g:nnn#set_default_mappings = 0

" Colors

colorscheme tokyonight

" Fileype specific settings

" Automatically deletes all tralling whitespace on save.
autocmd BufWritePre * %s/\s\+$//e
" Tab settings
autocmd Filetype css setlocal tabstop=2
autocmd FileType yml setlocal ai ts=2 sw=2 et
autocmd FileType yml setlocal ai ts=2 sw=2 et
autocmd FileType yaml setlocal ai ts=2 sw=2 et
autocmd FileType jinja setlocal ai ts=2 sw=2 et
autocmd FileType python setlocal ai ts=4 sw=4 et
autocmd FileType make setlocal noexpandtab

" Remaps

" Save
nmap <Leader>, :w<cr>
" New tab
nnoremap <C-t> :tabnew<cr>
" Exit Normal mode
inoremap jk <Esc>
" Spell-check
map <F6> :setlocal spell! spelllang=en_ca<CR>
" Replace all is aliased to S.
nnoremap S :%s//g<Left><Left>
vnoremap S noop
vnoremap S :s//g<Left><Left>
" Indent blocks in visual mode.
vnoremap K xkP`[V`]
vnoremap J xp`[V`]
vnoremap L >gv
vnoremap H <gv
" Toggle line chars view
noremap <Leader>l :set list!<CR>
" Snippets jumping
imap <silent><expr> <c-j> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<c-j>'
inoremap <silent> <c-k> <cmd>lua require'luasnip'.jump(-1)<Cr>
snoremap <silent> <c-j> <cmd>lua require('luasnip').jump(1)<Cr>
snoremap <silent> <c-k> <cmd>lua require('luasnip').jump(-1)<Cr>
" Generate annotations
nnoremap <silent> <Leader>d <cmd>lua require('neogen').generate()<Cr>
nnoremap <silent> <Leader>dc <cmd>lua require('neogen').generate({ type = 'class' })<CR>
nnoremap <silent> <Leader>df <cmd>lua require('neogen').generate({ type = 'file' })<CR>
nnoremap <silent> <Leader>dt <cmd>lua require('neogen').generate({ type = 'type' })<CR>
" Start n³ in the current buffers directory
nnoremap <Leader>n :NnnPicker %:p:h<CR>
