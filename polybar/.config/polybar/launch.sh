#!/bin/bash
#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch in all monitors
MONS=$(polybar -m | sed -e 's/:.*$//g')
for MON in ${MONS[@]}
do
	#echo $MON
	MONITOR=$MON polybar mybar -q -c ~/.config/polybar/mybar &
	echo $MONITOR
	#polybar example
done

echo "Bar launched..."
