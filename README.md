# dotfiles

These are my *personal* dotfiles and they work for me. I know they are not optimal and there are ways to implement a lot of the functionality better ... I just haven't gotten around to it.

Chances are once you install them, you will need to modify a few things to get it working perfectly, this is normal and probably will give you a chance to customize things per your liking.

## Installation
If you like a module or two (polybar, bspwm, ranger etc), you can install the configs by running:
`stow -v -t <TARGET_DIR> <MODULE_NAME(S)>` from the dotfiles directory.

eg, If you want polybar and ranger only:
`stow -v -t ~ polybar ranger`
This will install polybar and ranger in `/home/<user>/.config/polybar|ranger`.

> Many of the modules depend on helper scripts. These are all (for now) under the Scripts modules. You can install it the same as other modules. Once installed make sure that it is in your PATH. See the `profile_min/.profile` file for an example. You can also do this in another way (bashrc etc.)

## Notes for specific modules
### Tmux
after you run the stow command, you need to do a couple more things:
1. install tpm in the stowed directory:
git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm
2. create a sym link for the user config in home:
ln -s ~/.config/tmux/tmux.conf ~/.tmux.conf
3. source the config
tmux source ~/.tmux.conf
4. once in tmux, install all the plugins
(Prefix then Install) i.e 'Ctrl + a' then 'I'

###
